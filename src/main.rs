use std::time::Duration;

use tetra::graphics::animation::Animation;
use tetra::graphics::scaling::{ScalingMode, ScreenScaler};
use tetra::graphics::{self, Color, DrawParams, Rectangle, Texture};
use tetra::input::{self, Key};
use tetra::math::Vec2;
use tetra::time;
use tetra::{Context, ContextBuilder, Event, State};

type V2 = Vec2<f32>;
const ZERO: V2 = V2::new(0.0, 0.0);

const VIEW_SIZE: (i32, i32) = (320, 180);

#[derive(PartialEq)]
enum PlayerState {
    Idle,
    Attacking,
    Rolling,
    Running,
}

#[derive(PartialEq, Eq, Copy, Clone)]
#[repr(u8)]
enum Dir {
    Up,
    Down,
    Left,
    Right,
}

struct PlayerAnimation {
    state: PlayerState,
    dir: Dir,
    idle: [Animation; 4],
    attacking: [Animation; 4],
    rolling: [Animation; 4],
    running: [Animation; 4],
}

trait VecExt {
    fn move_toward(&mut self, target: Self, dist: f32);
}

impl VecExt for V2 {
    fn move_toward(&mut self, target: Self, dist: f32) {
        debug_assert!(dist >= 0.0);
        let diff = target - *self;
        let diff_mag = diff.magnitude();
        *self = if diff_mag == 0.0 || diff_mag < dist {
            target
        } else {
            *self + diff * (dist / diff_mag)
        };
    }
}

// Player constants.
const P_SPRITE_SIZE: f32 = 64.0;
/// Pixels per second per second.
const P_ACCEL: f32 = 500.0;
const P_DECEL: f32 = 500.0;
/// Pixels per second.
const P_MAX_SPEED: f32 = 100.0;

impl PlayerAnimation {
    fn new(ctx: &mut Context) -> tetra::Result<PlayerAnimation> {
        let texture = Texture::new(ctx, "./resources/Player/Player.png")?;
        let animation = move |start: usize, count: usize| {
            Animation::new(
                texture.clone(),
                Rectangle::row(0.0, 0.0, P_SPRITE_SIZE, P_SPRITE_SIZE)
                    .skip(start)
                    .take(count)
                    .collect(),
                Duration::from_secs_f64(0.1),
            )
        };
        let idle = |start: usize| animation(start, 1);
        let attacking = |start: usize| animation(24 + start, 4);
        let rolling = |start: usize| animation(40 + start, 5);
        let running = |start: usize| animation(start, 6);

        Ok(PlayerAnimation {
            state: PlayerState::Idle,
            dir: Dir::Right,
            idle: [idle(6), idle(18), idle(12), idle(0)],
            attacking: [attacking(4), attacking(12), attacking(8), attacking(0)],
            rolling: [rolling(5), rolling(15), rolling(10), rolling(0)],
            running: [running(6), running(18), running(12), running(0)],
        })
    }

    fn draw<P>(&self, ctx: &mut Context, params: P)
    where
        P: Into<DrawParams>,
    {
        self.current().draw(ctx, params)
    }

    fn current(&self) -> &Animation {
        match self.state {
            PlayerState::Idle => &self.idle[self.dir as usize],
            PlayerState::Attacking => &self.attacking[self.dir as usize],
            PlayerState::Rolling => &self.rolling[self.dir as usize],
            PlayerState::Running => &self.running[self.dir as usize],
        }
    }

    fn current_mut(&mut self) -> &mut Animation {
        match self.state {
            PlayerState::Idle => &mut self.idle[self.dir as usize],
            PlayerState::Attacking => &mut self.attacking[self.dir as usize],
            PlayerState::Rolling => &mut self.rolling[self.dir as usize],
            PlayerState::Running => &mut self.running[self.dir as usize],
        }
    }

    fn advance(&mut self, ctx: &Context) {
        self.current_mut().advance(ctx);
    }

    fn set_state(&mut self, state: PlayerState) {
        if self.state != state {
            self.state = state;
            self.current_mut().restart();
        }
    }
}

struct GameState {
    scaler: ScreenScaler,
    animation: PlayerAnimation,
    position: V2,
    velocity: V2,
}

impl GameState {
    fn new(ctx: &mut Context) -> tetra::Result<GameState> {
        Ok(GameState {
            scaler: ScreenScaler::with_window_size(
                ctx,
                VIEW_SIZE.0,
                VIEW_SIZE.1,
                ScalingMode::ShowAll,
            )?,
            animation: PlayerAnimation::new(ctx)?,
            position: V2::new(240.0, 160.0),
            velocity: ZERO,
        })
    }
}

/// Returns an input vector with magnitude at most 1.0.
fn get_input_vec(ctx: &mut Context) -> V2 {
    let mut input_vec = ZERO;
    if input::is_key_down(ctx, Key::W) {
        input_vec.y -= 1.0;
    }
    if input::is_key_down(ctx, Key::S) {
        input_vec.y += 1.0;
    }
    if input::is_key_down(ctx, Key::A) {
        input_vec.x -= 1.0;
    }
    if input::is_key_down(ctx, Key::D) {
        input_vec.x += 1.0;
    }
    if input_vec != ZERO {
        input_vec.normalize();
    }
    input_vec
}

impl State for GameState {
    fn update(&mut self, ctx: &mut Context) -> tetra::Result {
        let input_vec = get_input_vec(ctx);
        let delta = time::get_delta_time(ctx).as_secs_f32();

        // Update the direction to match input direction (even if the current
        // motion hasn't yet caught up) to give an immediacy of response.
        if input_vec != ZERO {
            let (x, y) = (input_vec.x, input_vec.y);
            self.animation.dir = match (y.abs() > x.abs(), x < 0.0, y < 0.0) {
                (true, _, true) => Dir::Up,
                (true, _, false) => Dir::Down,
                (false, true, _) => Dir::Left,
                (false, false, _) => Dir::Right,
            };
            self.velocity.move_toward(input_vec * P_MAX_SPEED, P_ACCEL * delta);
        } else {
            self.velocity.move_toward(ZERO, P_DECEL * delta);
        }

        self.position += self.velocity * delta;
        self.animation.set_state(if self.velocity == ZERO {
            if input::is_key_down(ctx, Key::E) {
                PlayerState::Attacking
            } else {
                PlayerState::Idle
            }
        } else if input::is_key_down(ctx, Key::LeftCtrl) {
            PlayerState::Rolling
        } else {
            PlayerState::Running
        });

        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> tetra::Result {
        self.animation.advance(ctx);

        graphics::set_canvas(ctx, self.scaler.canvas());
        graphics::clear(ctx, Color::rgb(0.094, 0.11, 0.16));

        self.animation.draw(
            ctx,
            DrawParams::new()
                .position(self.position)
                .origin(V2::new(P_SPRITE_SIZE / 2.0, P_SPRITE_SIZE / 2.0)),
        );

        graphics::reset_canvas(ctx);
        graphics::clear(ctx, Color::BLACK);

        self.scaler.draw(ctx);

        Ok(())
    }

    fn event(&mut self, _: &mut Context, event: Event) -> tetra::Result {
        if let Event::Resized { width, height } = event {
            self.scaler.set_outer_size(width, height);
        }

        Ok(())
    }
}

fn main() -> tetra::Result {
    let (width, height) = (VIEW_SIZE.0 * 4, VIEW_SIZE.1 * 4);
    ContextBuilder::new("Controlling Animations", width, height)
        .quit_on_escape(true)
        .resizable(true)
        .build()?
        .run(GameState::new)
}
